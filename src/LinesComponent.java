import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinesComponent extends JComponent {

    public static class Line {
        final double x1;
        final double y1;
        final double x2;
        final double y2;
        final Color color;
        public static final Color DEFAULT = Color.black;

        public Line(double x1, double y1, double x2, double y2) {
            this(x1, y1, x2, y2, DEFAULT);
        }

        public Line(double x1, double y1, double x2, double y2, Color color) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.color = color;
        }
    }

    public static class Point {
        final double x;
        final double y;
        final Color color;
        public static final Color DEFAULT = Color.black;

        public Point(double x, double y) {
            this(x, y, DEFAULT);
        }

        public Point(double x, double y, Color color) {
            this.x = x;
            this.y = y;
            this.color = color;
        }
    }

    private ArrayList<Line> lines = new ArrayList<>();
    private ArrayList<Point> points = new ArrayList<>();

    public void setLines(ArrayList<Line> lines) {
        this.lines = lines;
        repaint();
    }

    public void setPoints(ArrayList<Point> points) {
        this.points = points;
        repaint();
    }

    public double displayWidth = 240;
    public double displayHeight = 700;

    public static final int MARGIN_LEFT = 35;
    public static final int MARGIN_TOP = 35;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < lines.size(); ++i) {
            Line line = lines.get(i);
            g.setColor(line.color);
            g.drawLine(MARGIN_LEFT + (int) (line.x1 * displayWidth)
                     , MARGIN_TOP + (int) (displayHeight - line.y1 * displayHeight)
                     , MARGIN_LEFT + (int) (line.x2 * displayWidth)
                     , MARGIN_TOP + (int) (displayHeight - line.y2 * displayHeight));
        }
        for (int i = 0; i < points.size(); ++i) {
            Point p = points.get(i);
            g.setColor(p.color);
            g.drawRect(MARGIN_LEFT + (int) (p.x * displayWidth)
                     , MARGIN_TOP + (int) (displayHeight - p.y * displayHeight)
                     , 0, 0);
        }
    }

    public JFrame getFrame() {
        return frame;
    }

    private JFrame frame;

    public static LinesComponent makeComponent() {
        JFrame testFrame = new JFrame();
        testFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        final LinesComponent comp = new LinesComponent();
        comp.frame = testFrame;
        comp.setPreferredSize(new Dimension(1024, 800));
        testFrame.getContentPane().add(comp, BorderLayout.CENTER);
        testFrame.pack();
        testFrame.setVisible(true);
        return comp;
    }


}