import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey on 28.10.2014.
 */
public class BifurcationPlotter {
    public static final double R_STEP = 0.05;

    public static void main(String[] args) {
        LinesComponent component = LinesComponent.makeComponent();

        double r1 = Solution.findR1();
        System.out.println("r*    ~= " + r1);

        BifurcationPlotter instance = new BifurcationPlotter();

        ArrayList<LinesComponent.Line> plot = new ArrayList<>();
        double r3 = instance.makePlot(R_STEP, plot);
        System.out.println("r_inf ~= " + r3);
        component.setLines(plot);
        component.setPoints(instance.makeChaos(r3, 0.0025, 160));
        component.repaint();
    }

    private List<Double> currentLimits = new ArrayList<>();
    public static final double R0 = 1.001;


    private void addLinesBetweenLimits(List<LinesComponent.Line> to, List<Double> oldLimits, List<Double> newLimits, double oldR, double newR) {
        if (oldLimits.size() == newLimits.size()) {
            for (int i = 0; i < oldLimits.size(); ++i)
                to.add(new LinesComponent.Line(oldR, oldLimits.get(i), newR, newLimits.get(i)));
        } else if (oldLimits.size() * 2 == newLimits.size()) {
            for (int i = 0; i < oldLimits.size(); ++i) {
                to.add(new LinesComponent.Line(oldR, oldLimits.get(i), newR, newLimits.get(2 * i    )));
                to.add(new LinesComponent.Line(oldR, oldLimits.get(i), newR, newLimits.get(2 * i + 1)));
            }
        }
    }

    public static final double STEP_THRESHOLD = 1.3;
    public static final double STEP_MULTIPLIER = 0.1;

    //returns r***
    public double makePlot(double rStep, ArrayList<LinesComponent.Line> result) {
        currentLimits.clear();
        double r = R0;
        result.add(new LinesComponent.Line(0., 0., 1., 0., Color.black));
        for (; currentLimits.size() < Solution.CHECKS_FOR_LIMIT; r += rStep) {
            List<Double> solutionSequence = Solution.xSequence(r);
            List<Double> nextLimits = Solution.findLimits(solutionSequence);
            nextLimits.sort(Double::compare);
            if (!isPowerOf2(nextLimits.size()))
                break;
            addLinesBetweenLimits(result, currentLimits, nextLimits, r - rStep, r);
            currentLimits = nextLimits;
            if (r > STEP_THRESHOLD && r - rStep < STEP_THRESHOLD)
                rStep *= STEP_MULTIPLIER;
        }
        System.out.println("Limits count near r***: " + currentLimits.size());
        return r - rStep;
    }


    static boolean isPowerOf2(int n) {
        while (n != 1) {
            if ((n & 1) == 1)
                return false;
            n >>= 1;
        }
        return true;
    }

    public ArrayList<LinesComponent.Point> makeChaos(double from, double rStep, int steps) {
        ArrayList<LinesComponent.Point> result = new ArrayList<>();
        for (double r = from; steps > 0; r += rStep, steps--) {
            List<Double> limits = Solution.findLimits(Solution.xSequence(r), 300);
            for (double lim : limits) {
                result.add(new LinesComponent.Point(r, lim));
            }
        }
        return result;
    }
}
