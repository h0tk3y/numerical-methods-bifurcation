import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey on 29.10.2014.
 */
public class InteractivePlotter {

    public static void main(String[] args) {
        LinesComponent component = LinesComponent.makeComponent();
        component.displayWidth = 900;

        double r = 1.5;
        component.setLines(getLines(r, Solution.X0));
        JTextField rField = new JTextField();
        rField.setText("1.5");
        rField.setPreferredSize(new Dimension(100, 35));
        JButton rButton = new JButton();
        rButton.setText("Set R");

        JTextField xField = new JTextField();
        xField.setText(Double.toString(Solution.X0));
        xField.setPreferredSize(new Dimension(100, 35));
        JButton xButton = new JButton();
        xButton.setText("Set x0");

        JPanel interactionPanel = new JPanel();
        interactionPanel.add(rField);
        interactionPanel.add(rButton);

        interactionPanel.add(xField);
        interactionPanel.add(xButton);


        AbstractAction l = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double d = Double.parseDouble(rField.getText());
                    double x = Double.parseDouble(xField.getText());
                    component.setLines(getLines(d, x));
                } catch (NumberFormatException ignored) {
                }
            }
        };
        xButton.addActionListener(l);
        rButton.addActionListener(l);

        component.getFrame().getContentPane().add(interactionPanel, BorderLayout.SOUTH);
        component.getFrame().pack();
        component.repaint();
    }

    private static ArrayList<LinesComponent.Line> getLines(double r, double x) {
        List<Double> list = Solution.xSequence(r, x);
        ArrayList<LinesComponent.Line> lines = new ArrayList<>();
        for(int i = 1; i < list.size() - 1; i++) {
            double x0 = list.get(i - 1);
            double x1 = list.get(i);
            double x2 = list.get(i + 1);
            lines.add(new LinesComponent.Line(x0, x1, x1, x1, Color.RED));
            lines.add(new LinesComponent.Line(x1, x1, x1, x2, Color.RED));
        }
        lines.addAll(getGraph(r));
        lines.add(new LinesComponent.Line(0, 0, 1, 1));
        lines.add(new LinesComponent.Line(0, 0, 1, 0));
        return lines;
    }

    static ArrayList<LinesComponent.Line> getGraph(double r) {
        ArrayList<LinesComponent.Line> result = new ArrayList<>();
        double formerX, letterX, formerY, letterY;
        formerX = 0;
        formerY = Solution.nextX(formerX, r);
        letterX = 0;
        while (letterX <= 1) {
            letterX = formerX + 0.01;
            letterY = Solution.nextX(letterX, r);
            result.add(new LinesComponent.Line(formerX, formerY, letterX, letterY));
            formerX = letterX;
            formerY = letterY;
        }
        return result;
    }

}
