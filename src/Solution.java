import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey on 28.10.2014.
 */
public class Solution {

    public static double nextX(double x, double r) {
        return r * x * (1 - x);
    }

    public static final int ITERATIONS_COUNT = 30000;
    public static final double X0 = 0.5;

    static List<Double> xSequence(double r) {
        return xSequence(r, X0);
    }

    static List<Double> xSequence(double r, double x0) {
        List<Double> result = new ArrayList<Double>();
        result.add(x0);
        double next, lastX;
        for (int i = 0; i < ITERATIONS_COUNT; ++i) {
            lastX = result.get(result.size() - 1);
            next = nextX(lastX, r);
            result.add(next);
        }
        return result;
    }

    public static final int CHECKS_FOR_LIMIT = 1000;
    public static double LIMIT_EPSILON = 1e-10;

    static List<Double> findLimits(List<Double> sequence) {
        return findLimits(sequence, CHECKS_FOR_LIMIT);
    }

    static List<Double> findLimits(List<Double> sequence, int count) {
        List<Double> result = new ArrayList<Double>();
        for (int i = sequence.size() - 1; i >= sequence.size() - count; --i) {
            double x = sequence.get(i);
            if (!result.stream().anyMatch((Double lim) -> Math.abs(lim - x) < LIMIT_EPSILON)) {
                result.add(x);
            }
        }
        return result;
    }

    public static final double R1_LIMIT_EPSILON = 1e-5;

    static double findR1() {
        double l = 0.5;
        double r = 1.2;
        for (int i = 0; i < 20; ++i) {
            double mid = l + (r - l) / 2;
            List<Double> sequence = xSequence(mid);
            List<Double> limits = findLimits(sequence);
            if (limits.get(limits.size() - 1) < R1_LIMIT_EPSILON)
                l = mid;
            else
                r = mid;
        }
        return l;
    }

    private static boolean isMonotonous(List<Double> lst) {
        double diff = lst.get(1) - lst.get(0);
        for (int i = 1; i<lst.size() - 1; ++i) {
            double nextDiff = lst.get(i+1) - lst.get(i);
            if (diff * nextDiff < 0)
                return false;
            diff = nextDiff;
        }
        return true;
    }
}
